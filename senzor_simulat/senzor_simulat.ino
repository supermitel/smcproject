#define N_DEMO_MESSAGES 10
#define COM_PIN 7
#define COM_PORT PORTD

struct Pulse;
struct Message;
Message *currentMessage;
bool transmissionStatus = 0;


void transmission(int val)
{
  TIMSK2 = (val << OCIE2A);
  transmissionStatus = val;
  digitalWrite(13, !val);
}

struct Pulse{
  bool val;
  int duration;
  Pulse(bool val = true, int duration = 50):val(val), duration(duration){}
  void send(){
    COM_PORT = (COM_PORT & ~(1 << COM_PIN) | val << COM_PIN);
    OCR2A = duration * 2;
  }
};

struct Message{
  unsigned short int humid, humidDec = 0, temp, tempDec = 0;
  byte checksum = 0;
  int ibuff = 0, nbuff = 0;
  Pulse buff[128];
  Message(short int humid = 0,short int temp = 0):humid(humid), temp(temp)
  {
    humidDec = tempDec = 0;
    calcChecksum();
    generateBuffer();
    ibuff = 0;
  }
  Message(short int humid, unsigned short int humidDec, short int temp, unsigned short int tempDec):humid(humid), humidDec(humidDec), temp(temp), tempDec(tempDec)
  {
    calcChecksum();
    generateBuffer();
    ibuff = 0;
  }
  void addPulseByteToBuffer(byte val)
  {
    for(int i = 7; i >= 0; i--)
    {
      buff[nbuff++] = Pulse(0, 50);
      buff[nbuff++] = (val & 1 << i)?Pulse(1, 70):Pulse(1, 27);
    }
  }
  void generateBuffer()
  {
    nbuff = 2;
    buff[0] = Pulse(0, 80);
    buff[1] = Pulse(1, 80);
    addPulseByteToBuffer(humid);
    addPulseByteToBuffer(humidDec);
    addPulseByteToBuffer(temp);
    addPulseByteToBuffer(tempDec);
    if(!checksum) calcChecksum();
    addPulseByteToBuffer(checksum);
    buff[nbuff++] = Pulse(0, 50);
  }
  void calcChecksum()
  {
    checksum = humid+humidDec+temp+tempDec;
  }
  void sendNextPulse()
  {
    if(ibuff >= nbuff)
      transmission(false);
    else
      buff[ibuff++].send();
  }
  void send()
  {
    while(transmissionStatus);
    pinMode(COM_PIN, OUTPUT);
    ibuff = 0;
    currentMessage = this;
    transmission(true);
   }
};

ISR(TIMER2_COMPA_vect){
 currentMessage->sendNextPulse();
}


void w8ForSignal(){
  int C_PIN = 7;
 // while(transmissionStatus);
  digitalWrite(C_PIN, HIGH);
  pinMode(C_PIN, INPUT);
  digitalWrite(C_PIN, HIGH);
  digitalWrite(COM_PIN, HIGH);
  while(digitalRead(C_PIN));
  while(!digitalRead(C_PIN));
  delayMicroseconds(50);
}


//Message demoMessages[N_DEMO_MESSAGES];
//void initDemoMessages()
//{
//  for(int i = 0; i < N_DEMO_MESSAGES; i++)
//    demoMessages[i] = Message(random(0, 100), random(0, 50));
//}

void setup() {
  
  pinMode(COM_PIN, OUTPUT);
  pinMode(13, OUTPUT);
  randomSeed(analogRead(A0));
  digitalWrite(13, HIGH);
  
  TCCR2A = 0;
  TCCR2B = 0;
  TCNT2  = 0;
  OCR2A = 146; //tick la 0.5*4 = 2us
  TCCR2A |= (1 << WGM01);
  TCCR2B |= (1 << CS01); // prescaler 8 -> 0.5 us  
  TIMSK2 = 0;

//  initDemoMessages();
}

void loop() {
  if(!transmissionStatus)
  {
    w8ForSignal();
    Message(random(0, 100), random(0, 50)).send();
  }
  //Message(random(0, 100), random(0, 50)).send();
}
 
