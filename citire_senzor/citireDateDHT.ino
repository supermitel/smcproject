#include <LiquidCrystal.h>

#define DDD2 2
#define PD2 2
#define RS 9
#define E 8
#define D4 7
#define D5 6
#define D6 5
#define D7 4

byte receivedData = 0x00;
char message1[] = "Temp = 00.0 C";
char message2[] = "RH   = 00.0 %";
unsigned short TOUT = 0, CheckSum, i;
unsigned short T_Byte1, T_Byte2, RH_Byte1, RH_Byte2;

LiquidCrystal LCD(RS, E, D4, D5, D6, D7);

void StartSignal()
{
  DDRD |= (1 << PD2);   //Pinul 2 este de output.
  PORTD &= ~(1 << PD2); //Pus pe 0.
  
  delayMicroseconds(20); //Daca este tinut pe mai mult de 18ms inseamna ca senzorul va trebui sa dea date.
  PORTD |= (1 << PD2);   //Dupa 25ms este pus pe 1.
  
  delayMicroseconds(30); //Se tine 1 intre 20-40ms si apoi devine de input pentru ca datele sa vina de la senzor.
  DDRD &= ~(1 << DDD2);  // Pinul 2 este de input.
}

unsigned short ReadByte()
{
  unsigned short num = 0, t;
  DDRD &= ~(1 << PD2);
  
  for (i = 0; i < 8; i++)
  {
    while(!(PIND & (1 << PD2)));
    delayMicroseconds(30);
    
    if(PIND & (1 << PD2))
    {
      num |= 1<<(7-i);
    }
    
    while((PIND & (1 << PD2)));
    delayMicroseconds(40);
  }
  
  return num;
}


void setup()
{
  LCD.begin(16, 2);
}

void loop()
{
  delay(1000);
  StartSignal();
  
  RH_Byte1 = ReadByte();
  RH_Byte2 = ReadByte();
  T_Byte1 = ReadByte();
  T_Byte2 = ReadByte();

  message1[7]  = T_Byte1/10 + 48;
  message1[8]  = T_Byte1%10 + 48;
  message1[10] = T_Byte2/10 + 48;
  //message2[7]  = RH_Byte1/10 + 48;
  message2[8]  = RH_Byte1%10 + 48;
  message2[10] = RH_Byte2/10 + 48;
  message1[11] = 223; // Degree symbol
  
  LCD.clear();
  LCD.setCursor(0, 0);
  LCD.print(message1);
  LCD.setCursor(0, 1);
  LCD.print(message2);
}
